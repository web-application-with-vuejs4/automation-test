<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Username  Password     .  .            _c37020</name>
   <tag></tag>
   <elementGuidId>cf7b09cf-c31e-416a-8152-d0ab3556295a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='page-wrapper']/div/div/div/div/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.card-body</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>card-body</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
        		
                
                
                    
                         การเข้าใช้งานระบบด้วย Username และ Password ของท่านถือว่าเป็นการลงลายมือชื่อทางอิเล็กทรอนิกส์ ตามมาตรา ๙ ของ พ.ร.บ. ธุรกรรมทางอิเล็กทรอนิกส์ พ.ศ. ๒๕๔๔ อ่านเพิ่มเติม 
                
            
                
                
                
                
                    บัญชีผู้ใช้ (Username)
                    
                
                
                    รหัสผ่าน (Password)
                    
					Show Password
                
                
                    เข้าสู่ระบบ / Sign in  
							ลืมรหัสผ่าน / reset password
						
                
                
                
                                    
                        รหัสผ่านไม่ถูกต้อง
                        Password incorrect!
                    
                                  
            
            
			วีดีโอสาธิตการใช้งานระบบ myid ใช้งานได้ทั้งนิสิต อาจารย์และบุคลากร
            
            
            
            	
                
                
            
        </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;page-wrapper&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-lg-12&quot;]/div[1]/div[@class=&quot;card&quot;]/div[@class=&quot;card-body&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='page-wrapper']/div/div/div/div/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='เข้าสู่ระบบ (Sign In)'])[1]/following::div[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='วีดีโอสาธิตการใช้งานระบบ myid ใช้งานได้ทั้งนิสิต อาจารย์และบุคลากร']/parent::*</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/div/div/div/div/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
        		
                
                
                    
                         การเข้าใช้งานระบบด้วย Username และ Password ของท่านถือว่าเป็นการลงลายมือชื่อทางอิเล็กทรอนิกส์ ตามมาตรา ๙ ของ พ.ร.บ. ธุรกรรมทางอิเล็กทรอนิกส์ พ.ศ. ๒๕๔๔ อ่านเพิ่มเติม 
                
            
                
                
                
                
                    บัญชีผู้ใช้ (Username)
                    
                
                
                    รหัสผ่าน (Password)
                    
					Show Password
                
                
                    เข้าสู่ระบบ / Sign in  
							ลืมรหัสผ่าน / reset password
						
                
                
                
                                    
                        รหัสผ่านไม่ถูกต้อง
                        Password incorrect!
                    
                                  
            
            
			วีดีโอสาธิตการใช้งานระบบ myid ใช้งานได้ทั้งนิสิต อาจารย์และบุคลากร
            
            
            
            	
                
                
            
        ' or . = '
        		
                
                
                    
                         การเข้าใช้งานระบบด้วย Username และ Password ของท่านถือว่าเป็นการลงลายมือชื่อทางอิเล็กทรอนิกส์ ตามมาตรา ๙ ของ พ.ร.บ. ธุรกรรมทางอิเล็กทรอนิกส์ พ.ศ. ๒๕๔๔ อ่านเพิ่มเติม 
                
            
                
                
                
                
                    บัญชีผู้ใช้ (Username)
                    
                
                
                    รหัสผ่าน (Password)
                    
					Show Password
                
                
                    เข้าสู่ระบบ / Sign in  
							ลืมรหัสผ่าน / reset password
						
                
                
                
                                    
                        รหัสผ่านไม่ถูกต้อง
                        Password incorrect!
                    
                                  
            
            
			วีดีโอสาธิตการใช้งานระบบ myid ใช้งานได้ทั้งนิสิต อาจารย์และบุคลากร
            
            
            
            	
                
                
            
        ')]</value>
   </webElementXpaths>
</WebElementEntity>
